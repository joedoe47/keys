# Keys

This is a list of public keys that can be used to securely contact me.

Personal Keys:

1- keybase.asc

2- Yubikey.asc

3- Malvalope.asc

4- trezor.asc (rarely used but available)


Work Keys:

1- work.asc
